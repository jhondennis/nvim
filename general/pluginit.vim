"===================================================================
"===                PLUGINS INSTALLED                            ===
"===================================================================
let $VIMHOME=expand('<sfile>:p:h:h')

call plug#begin($VIMHOME . '/plugins')

" File system explorer and complements
  Plug 'preservim/nerdtree'
" Icons for nerdtree
  Plug 'ryanoasis/vim-devicons'
" Icons status git on nerdtree
  " Plug 'xuyuanp/nerdtree-git-plugin'
" It show which lines have been added, modified or removed
  Plug 'airblade/vim-gitgutter'

" Lean & mean status/tabline for vim that's light as air.
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

" Commenter line
  Plug 'aluriak/nerdcommenter'

" Auto complete with languge server
  Plug 'neoclide/coc.nvim', { 'branch': 'release'}

" Git commands on vim
  " Plug 'tpope/vim-fugitive'

" Support for expanding abbreviations
  Plug 'mattn/emmet-vim'

" Insert or delete brackets, parens, quotes in pair
  Plug 'jiangmiao/auto-pairs'
" Text object plugin to add, delete, replace brackets
  Plug 'machakann/vim-sandwich'
" Is all about surrounding (), {}, '', xml, tags and more
  Plug 'tpope/vim-surround'

" post install (yarn install | npm install) then load plugin only for editing supported files
  Plug 'prettier/vim-prettier', {
   \ 'do': 'npm install --frozen-lockfile --production',
   \ 'for': ['javascript', 'typescript', 'css', 'scss', 'json', 'vue', 'jsx', 'yaml', 'html'] }
" Displaying thin vertical lines at each indentation
  Plug 'Yggdroot/indentLine'

" Multiple cursos for editing
  Plug 'terryma/vim-multiple-cursors'

" Visual plugins and themes
  Plug 'sainnhe/sonokai'
  " Plug 'ayu-theme/ayu-vim'
  " Plug 'sainnhe/gruvbox-material'
  " Plug 'sainnhe/edge'

" Collection language packs for vim
  Plug 'sheerun/vim-polyglot'

" Funzi file search
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'

" File search AUN NO FUNCIONA - EN WINDOWS ME SALE ERROR
  " Plug 'nvim-lua/popup.nvim'
  " Plug 'nvim-lua/plenary.nvim'
  " Plug 'nvim-telescope/telescope.nvim'

" Vim close tag
  Plug 'alvan/vim-closetag'

call plug#end()
