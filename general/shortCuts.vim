"===================================================================
"===                      SHORTCUTS CONFIG                       ===
"===================================================================

" :h key-notation  --> para ver la notacion de las teclas

" -------------------------------------------------------------------
let mapleader = " "
nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <F5> :source %<CR>
vmap <F5> :source %<CR>
imap <silent> ii <Esc>

" -------------------------------------------------------------------
nnoremap <silent> <C-Right> :vertical resize +3 <CR>
nnoremap <silent> <C-Left>  :vertical resize -3 <CR>
nnoremap <silent> <C-Up>    :resize -3          <CR>
noremap  <silent> <C-Down>  :resize +3          <CR>

" -------------------------------------------------------------------
" Insert new line bottom
nnoremap <Leader>o o<Esc>
nnoremap <Leader><S-O><Esc>

" -------------------------------------------------------------------
" Mappings to move lines
nnoremap <A-S-j>      :m .+1  <CR>==
nnoremap <A-S-k>      :m .-2  <CR>==
inoremap <A-S-j> <Esc>:m .+1  <CR>==gi
inoremap <A-S-k> <Esc>:m .-2  <CR>==gi
vnoremap <A-S-j>      :m '>+1 <CR>gv=gv
vnoremap <A-S-k>      :m '<-2 <CR>gv=gv

" -------------------------------------------------------------------
" Move on Buffers
nmap <Leader>bp :bprevious<CR>
nmap <Leader>bn :bnext<CR>
nmap <Leader>bf :bfirst<CR>
nmap <Leader>bd :bdelete<CR>

" -------------------------------------------------------------------
" Move on Tabs
nmap <Leader>tp :tabprevious<CR>
nmap <Leader>tn :tabnext<CR>
nmap <Leader>tf :tabfirst<CR>
nmap <Leader>tc :tabclose<CR>

" -------------------------------------------------------------------
" Smart way to move between windows
" NOTE: this is replace with ma terminals
" map <C-j> <C-W>j
" map <C-k> <C-W>k
" map <C-h> <C-W>h
" map <C-l> <C-W>l

" -------------------------------------------------------------------
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
  noremap <Leader><Esc> :nohl<CR>
" vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
" vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>
