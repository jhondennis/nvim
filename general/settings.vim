"===================================================================
"===                      GENERAL CONFIG                         ===
"===================================================================

set encoding=UTF-8

filetype plugin indent on
syntax on

if has('clipboard')
  set clipboard+=unnamedplus
endif

"Restore cursor to file position in previous editing session
:au BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
\ |   exe "normal! g`\""
\ | endif

set scrolloff=10
set nobackup
set expandtab
set wildmenu
set completeopt=menuone,preview,noinsert
set bs=2
set hidden
set mouse=a
set mousehide
set incsearch             " highlight search string as search pattern is entered
set hlsearch              " disables last search hilighting
set number
set wrap
set backspace=indent,eol,start " Fixes common backspace problems
set laststatus=2          " Status bar
set showbreak=>>>\ \ \    " Wrap-broken line prefix
set textwidth=79          " Line wrap (number of cols)
set showmatch             " Highlight matching brace
set complete+=kspell      " text complete with CTRL-N or CTRL-P
set spelllang=en_us
set visualbell            " Use visual bell (no beeping)
set ignorecase            " Always case-insensitie
set smartcase             " Enable smart-case search
set autoindent            " Auto-indent new lines
set wildignorecase        " case insensitive auto completion
set shiftwidth=2          " Number of auto-indent spaces
set smartindent           " Enable smart-indent
set smarttab              " Enable smart-tabs
set softtabstop=2         " Number of spaces per Tab
set tabstop=2             " tabulaciones 2 espacios
set confirm               " Prompt confirmation dialogs
set ruler                 " Show row and column ruler information
set relativenumber        " Relative numbers in to left
set cmdheight=1           " Command line height
set autowriteall          " Auto-write all file changes
set list                  " Display unprintable characters f12 - switches
set listchars=tab:••\ ,trail:•,extends:»,precedes:« " Unprintable chars mapping
" Suppress appending <PasteStart> and <PasteEnd> when pasting
set t_BE=
set nosc noru nosm
" Highlights "{{{
" ---------------------------------------------------------------
set cursorline
set colorcolumn=100
" set cursorcolumn

" Set cursor line color on visual mode
" highlight ColorColumn ctermbg=236
" highlight Visual cterm=none ctermbg=236 ctermfg=none guibg=Grey40
" highlight LineNr cterm=none ctermfg=245 guifg=#2b506e guibg=#000000
" highlight CursorLine cterm=none ctermbg=238
" highlight CursorLineNr cterm=bold ctermfg=none
"}}}


" Line vertical right
" ---------------------------------------------------------------
highlight VertSplit ctermbg=236 ctermfg=236

" ---------------------------------------------------------------
" Enable folding
set foldmethod=marker         " za open-close
set foldlevel=99

"Enable Tags
set tags=tags

"Remove Trailing whitespaces in all files
autocmd BufWritePre * %s/\s\+$//e

