
"===================================================================
"===                      NERDTREE CONFIG                        ===
"===================================================================

" NERTree config

let NERDTreeAutoDeleteBuffer = 1
let NERDTreeShowLineNumbers = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeQuitOnOpen = 0
let g:NERDTreeWinSize = 40

nmap <Leader>nn :NERDTree           <CR>:NERDTreeRefreshRoot<CR>
nmap <Leader>nf :NERDTreeFind       <CR>:NERDTreeRefreshRoot<CR>
nmap <Leader>nt :NERDTreeToggle     <CR>:NERDTreeRefreshRoot<CR>
nmap <Leader>nc :NERDTreeClose      <CR>
nmap <Leader>nr :NERDTreeRefreshRoot<CR>
" map  <Leader>l gt<cr>
" map  <Leader>h gT<cr>

" Start NERDTree and leave the cursor in it.
autocmd VimEnter * NERDTree

" Start NERDTree and put the cursor back in the other window.
" autocmd VimEnter * NERDTree | wincmd p

" Start NERDTree when Vim is started without file arguments.
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

" Start NERDTree. If a file is specified, move the cursor to its window.
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif

" Exit Vim if NERDTree is the only window left.
" autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
"     \ quit | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
\ let buf=bufnr() | buffer# | execute 'normal! \<C-W>w' | execute 'buffer'.buf | endif

" let g:NetrwIsOpen=0

" function! ToggleNetrw()
"     if g:NetrwIsOpen
"         let i = bufnr("$")
"         while (i >= 1)
"             if (getbufvar(i, '&filetype') == 'netrw')
"                 silent exe 'bwipeout ' . i
"             endif
"             let i-=1
"         endwhile
"         let g:NetrwIsOpen=0
"     else
"         let g:NetrwIsOpen=1
"         silent Lexplore
"     endif
" endfunction
"
" noremap <silent> <F2> :call ToggleNetrw()<CR>
