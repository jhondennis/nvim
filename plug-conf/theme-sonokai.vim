
"===================================================================
"===                 SONOKAI THEME CONFIGURATIONS                ===
"===================================================================

" Important!!
if has('termguicolors')
  set termguicolors
endif

" The configuration options should be placed before `colorscheme sonokai`.
let g:lightline = {'colorscheme' : 'sonokai'}
"
let g:sonokai_style = 'default'
" let g:sonokai_style = 'atlantis'
" let g:sonokai_style = 'andromeda'
" let g:sonokai_style = 'shusia'
" let g:sonokai_style = 'maia'

let g:lightline.colorscheme = 'sonokai'

let g:airline_theme = 'sonokai'
let g:sonokai_cursor = 'green'
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 0
let g:sonokai_transparent_background = 1


