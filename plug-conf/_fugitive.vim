
"===================================================================
"===                    FUGITIVE CONFIGURATIONS                  ===
"===================================================================


" Fugitive git bindings
" nnoremap <Leader>ga :Git add %:p<CR><CR>
" nnoremap <Leader>ga :Git add %<Space>
nnoremap <Leader>gs :Gstatus<CR>
" nnoremap <Leader>gc :Gcommit -v -q<CR>
" nnoremap <Leader>gt :Gcommit -v -q %:p<CR>
" nnoremap <Leader>gd :Gdiff<CR>
" nnoremap <Leader>ge :Gedit<CR>
" nnoremap <Leader>gr :Gread<CR>
" nnoremap <Leader>gw :Gwrite<CR><CR>
" nnoremap <Leader>gl :silent! Glog<CR>:bot copen<CR>
" nnoremap <Leader>gp :Ggrep<Space>
" nnoremap <Leader>gm :Gmove<Space>
nnoremap <Leader>gb :Git branch<Space>
nnoremap <Leader>go :Git checkout<Space>
" nnoremap <Leader>gps :Dispatch! git push<CR>
" nnoremap <Leader>gpl :Dispatch! git pull<CR>


"" Git other configurations
" noremap <Leader>ga :Gwrite<CR>
" noremap <Leader>gc :Gcommit<CR>
" noremap <Leader>gsh :Gpush<CR>
" noremap <Leader>gll :Gpull<CR>
" noremap <Leader>gs :Gstatus<CR>
" noremap <Leader>gb :Gblame<CR>
" noremap <Leader>gd :Gvdiff<CR>
" noremap <Leader>gr :Gremove<CR>
