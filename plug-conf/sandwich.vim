"===================================================================
"===                      SANDWICH CONFIG                        ===
"===================================================================


" The point is that it would be nice to be shared the definitions of
" {surrounding}s pairs in all kinds of operations. User can freely
" add new settings to extend the functionality. If g:sandwich#recipes
" was defined, this plugin works with the settings inside. As a first
" step, it would be better to copy the default settings in g:sandwich#default_recipes.

" let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)

" Each setting, it is called recipe, is a set of a definition
" of {surrounding}s pair and options. The key named buns is used
" for the definition of {surrounding}.

" let g:sandwich#recipes += [{'buns': [{surrounding}, {surrounding}], 'option-name1': {value1}, 'option-name2': {value2} ...}]

" For example: {'buns': ['(', ')']}
"    foo   --->   (foo)

" Or there is a different way, use external textobjects to define {surrounding}s from the difference of two textobjects.

" let g:sandwich#recipes += [{'external': [{textobj-i}, {textobj-a}], 'option-name1': {value1}, 'option-name2': {value} ...}]

" For example: {'external': ['it', 'at']}
    " <title>foo</title>   --->   foo
