"===================================================================
"===                MULTIPLE CURSOR CONFIG                       ===
"===================================================================


" normal mode / visual mode
"
" start       : <C-n> start multicursor and add a virtual cursor + selection on the match
" next        : <C-n> add a new virtual cursor + selection on the next match
" skip        : <C-x> skip the next match
" prev        : <C-p> remove current virtual cursor + selection and go back on previous match
" select all  : <A-n> start multicursor and directly select all matches

let g:multi_cursor_use_default_mapping=0
" Default mapping
let g:multi_cursor_start_word_key      = '<C-n>'
" let g:multi_cursor_select_all_word_key = '<A-n>'
let g:multi_cursor_start_key           = 'g<C-n>'
" let g:multi_cursor_select_all_key      = 'g<A-n>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'

