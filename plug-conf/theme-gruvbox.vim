

"===================================================================
"===                GROUVBOX THEME CONFIGURATIONS                ===
"===================================================================

" Important!!
if has('termguicolors')
  set termguicolors
endif

" For dark version.
set background=dark

" For light version.
" set background=light


let g:gruvbox_material_enable_italic = 1
let g:gruvbox_material_disable_italic_comment = 0

" Set contrast.
" This configuration option should be placed before `colorscheme gruvbox-material`.
" Available values: 'hard', 'medium'(default), 'soft'
" Select one option
" let g:gruvbox_material_background = 'soft'
let g:gruvbox_material_background = 'hard'
" let g:gruvbox_material_background = 'medium'

let g:airline_theme = 'gruvbox_material'


