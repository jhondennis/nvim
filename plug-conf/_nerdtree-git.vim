"===================================================================
"===                 NERDTREE-GIT CONFIGURATIONS                 ===
"===================================================================

let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

" you should install nerdfonts by yourself. default: 0

let g:NERDTreeGitStatusUseNerdFonts = 0 " a heavy feature may cost much more time. default: 0

let g:NERDTreeGitStatusShowIgnored = 0

" How to indicate every single untracked file under an untracked dir?

" let g:NERDTreeGitStatusUntrackedFilesMode = 'all' " a heave feature too. default: normal

" How to set git executable file path?

" let g:NERDTreeGitStatusGitBinPath = '/your/file/path'  defualt: git (auto find in path)

" How to show Clean indicator?

let g:NERDTreeGitStatusShowClean = 1 " default: 0

" How to hide the boring brackets([ ])?

let g:NERDTreeGitStatusConcealBrackets = 0 " default: 0

