
"===================================================================
"===                    PRETTIER CONFIGURATION                   ===
"===================================================================

" Overwrite DEFAULT prettier configuration

" Note : vim-prettier default settings differ from prettier intentionally. However they can be configured by:

" Max line length that prettier will wrap on: a number or 'auto' (use
" textwidth).
" default: 'auto'
let g:prettier#config#print_width = 'auto'

" number of spaces per indentation level: a number or 'auto' (use
" softtabstop)
" default: 'auto'
let g:prettier#config#tab_width = 'auto'

" use tabs instead of spaces: true, false, or auto (use the expandtab setting).
" default: 'auto'
let g:prettier#config#use_tabs = 'auto'

" flow|babylon|typescript|css|less|scss|json|graphql|markdown or empty string
" (let prettier choose).
" default: ''
let g:prettier#config#parser = ''

" cli-override|file-override|prefer-file
" default: 'file-override'
let g:prettier#config#config_precedence = 'file-override'

" always|never|preserve
" default: 'preserve'
let g:prettier#config#prose_wrap = 'preserve'

" css|strict|ignore
" default: 'css'
let g:prettier#config#html_whitespace_sensitivity = 'css'

" false|true
" default: 'false'
let g:prettier#config#require_pragma = 'false'

" Define the flavor of line endings
" lf|crlf|cr|all
" defaut: 'lf'
let g:prettier#config#end_of_line = get(g:, 'prettier#config#end_of_line', 'lf')

" CONFIGURATION
"
" Change the mapping to run from the default of <Leader>p
"
" nmap <Leader>py <Plug>(Prettier)
"
" Enable auto formatting of files that have '@format' or '@prettier' tag
"
" let g:prettier#autoformat = 1
"
" Allow auto formatting for files without '@format' or '@prettier' tag
"
" let g:prettier#autoformat_require_pragma = 0

" NOTE The previous two options can be used together for autoformatting files on save without @format or @prettier tags
let g:prettier#autoformat = 1
let g:prettier#autoformat_require_pragma = 0

" Toggle the g:prettier#autoformat setting based on whether a config file can be found in the current directory or any parent directory. Note that this will override the g:prettier#autoformat setting!
"
" let g:prettier#autoformat_config_present = 1
"
" A list containing all config file names to search for when using the g:prettier#autoformat_config_present option.
"
" let g:prettier#autoformat_config_files = [...]
"
" Set the prettier CLI executable path
"
" let g:prettier#exec_cmd_path = '~/path/to/cli/prettier'
"
" The command :Prettier by default is synchronous but can also be forced async
"
" let g:prettier#exec_cmd_async = 1
"
" By default parsing errors will open the quickfix but can also be disabled
"
" let g:prettier#quickfix_enabled = 0
"
" By default selection formatting will be running :PrettierFragment but we can set :PrettierPartial as the default selection formatting by:
"
" let g:prettier#partial_format=1
"
" By default we auto focus on the quickfix when there are errors but can also be disabled
"
" let g:prettier#quickfix_auto_focus = 0
"
" To running vim-prettier not only before saving, but also after changing text or leaving insert mode:
"
" " when running at every change you may want to disable quickfix
" let g:prettier#quickfix_enabled = 0
"
" autocmd TextChanged,InsertLeave *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync
