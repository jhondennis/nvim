
"===================================================================
"===                 FZF CONFIG                                  ===
"===================================================================

" Command   "  Vim function
" --------- "--------------
" Files     "  fzf#vim#files(dir, [spec dict], [fullscreen bool])
" GFiles 	  "  fzf#vim#gitfiles(git_options, [spec dict], [fullscreen bool])
" GFiles?   "  fzf#vim#gitfiles('?', [spec dict], [fullscreen bool])
" Buffers   "  fzf#vim#buffers([spec dict], [fullscreen bool])
" Colors 	  "  fzf#vim#colors([spec dict], [fullscreen bool])
" Rg 	      "  fzf#vim#grep(command, [has_column bool], [spec dict], [fullscreen bool])

nnoremap <leader><tab> :GFiles<CR>
let g:fzf_preview_window = ['right:50%', 'ctrl-/']
" nnoremap <leader><tab>l :BLines<CR>  " Busca palabras dentro el archivo
"
" nnoremap <C-F> :Files<CR>    " incluye los node modules
" nnoremap <C-F>l :BLines<CR>
" Let's say you want to a variation of it called ProjectFiles that only searches inside ~/projects directory.
" Then you can do it like this
  " command! -bang ProjectFiles call fzf#vim#files(':cd %:h', <bang>0)

" -------------------------------- "----------------------------------
" Mapping                          "  Description
" -------------------------------- "----------------------------------
" <plug>(fzf-maps-n)               "  Normal mode mappings
" <plug>(fzf-maps-i)               " 	Insert mode mappings
" <plug>(fzf-maps-x)               " 	Visual mode mappings
" <plug>(fzf-maps-o)               " 	Operator-pending mappings
" <plug>(fzf-complete-word)        "  cat /usr/share/dict/words
" <plug>(fzf-complete-path)        "  Path completion using find (file + dir)
" <plug>(fzf-complete-file)        "  File completion using find
" <plug>(fzf-complete-line)        "  Line completion (all open buffers)
" <plug>(fzf-complete-buffer-line) " 	Line completion (current buffer only)

" Mapping selecting mappings
" nmap <leader><tab> <plug>(fzf-maps-n)
" xmap <leader><tab> <plug>(fzf-maps-x)
" omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
" imap <c-x><c-k> <plug>(fzf-complete-word)
" imap <c-x><c-f> <plug>(fzf-complete-path)
" imap <c-x><c-l> <plug>(fzf-complete-line)

" This is the default extra key bindings
" let g:fzf_action = {
"   \ 'ctrl-t': 'tab split',
"   \ 'ctrl-x': 'split',
"   \ 'ctrl-v': 'vsplit' }

" An action can be a reference to a function that processes selected lines
" function! s:build_quickfix_list(lines)
"   call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
"   copen
"   cc
" endfunction

" let g:fzf_action = {
"   \ 'ctrl-q': function('s:build_quickfix_list'),
"   \ 'ctrl-t': 'tab split',
"   \ 'ctrl-x': 'split',
"   \ 'ctrl-v': 'vsplit' }

" -------------------------------------------------------------------
" Default fzf layout -- this is for window buffer on the vim
" - Popup window
  " let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

" - down / up / left / right
  " let g:fzf_layout = { 'down': '40%' }

" - Window using a Vim command
  " let g:fzf_layout = { 'window': 'enew' }
  " let g:fzf_layout = { 'window': '-tabnew' }
  " let g:fzf_layout = { 'window': '10new' }
" -------------------------------------------------------------------

" Customize fzf colors to match your color scheme
" - fzf#wrap translates this to a set of `--color` options
" let g:fzf_colors =
" \ { 'fg':      ['fg', 'Normal'],
"   \ 'bg':      ['bg', 'Normal'],
"   \ 'hl':      ['fg', 'Comment'],
"   \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
"   \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
"   \ 'hl+':     ['fg', 'Statement'],
"   \ 'info':    ['fg', 'PreProc'],
"   \ 'border':  ['fg', 'Ignore'],
"   \ 'prompt':  ['fg', 'Conditional'],
"   \ 'pointer': ['fg', 'Exception'],
"   \ 'marker':  ['fg', 'Keyword'],
"   \ 'spinner': ['fg', 'Label'],
"   \ 'header':  ['fg', 'Comment'] }

" Enable per-command history
" - History files will be stored in the specified directory
" - When set, CTRL-N and CTRL-P will be bound to 'next-history' and
"   'previous-history' instead of 'down' and 'up'.
  " let g:fzf_history_dir = '~/.local/share/fzf-history'
