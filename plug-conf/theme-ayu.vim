
"===================================================================
"===                    AYU THEME  CONFIGURATIONS                ===
"===================================================================

" Important!!
if has('termguicolors')
  set termguicolors   " enable true colors support
endif


" let ayucolor="dark"   " for dark version of theme
" let ayucolor="light"  " for light version of theme
let ayucolor="mirage" " for mirage version of theme

" IndentLine {{
let g:indentLine_char = '|'
let g:indentLine_first_char = '¦'
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_setColors = 0
" }}
