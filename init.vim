
let $VIMHOME=expand('<sfile>:p:h:h')

"===================================================================
"===                 EDITOR CONFIGURATIONS                       ===
"===================================================================
    exec 'source ' $VIMHOME . '/nvim/general/settings.vim'

"===================================================================
"===                 SHORTCUTS                                   ===
"===================================================================

    exec 'source ' $VIMHOME . '/nvim/general/shortCuts.vim'

"===================================================================
"===                TERMINAL CONFIG                              ===
"===================================================================

    exec 'source ' $VIMHOME . '/nvim/general/terminal-vim.vim'

"===================================================================
"===                 SCRIPT OPEN VIMRC                           ===
"===================================================================
" Auto open file neovim|-->init.vim  vim|-->.vimrc
  let g:vimrc_path = expand('<sfile>')
  nnoremap <silent> <Leader>v :execute 'edit' fnameescape(g:vimrc_path)<CR>

"===================================================================
"===                 PLUGINS INSTALL                             ===
"===================================================================

    exec 'source ' $VIMHOME . '/nvim/general/pluginit.vim'

"===================================================================
"===                 PLUGS  CONFIGURATIONS                       ===
"===================================================================

    exec 'source ' $VIMHOME . '/plug-conf/nerdtree.vim'
  " exec 'source ' $VIMHOME . '/plug-conf/gitgutter.vim'

    exec 'source ' $VIMHOME . '/plug-conf/airline.vim'

    exec 'source ' $VIMHOME . '/plug-conf/nerdcommenter.vim'

    " Language server coc
    exec 'source ' $VIMHOME . '/plug-conf/cocvim.vim'

  " source $HOME/.config/nvim/plug-conf/fugitive.vim

    exec 'source ' $VIMHOME . '/plug-conf/closetag.vim'

    exec 'source ' $VIMHOME . '/plug-conf/emmet-vim.vim'

    exec 'source ' $VIMHOME . '/plug-conf/auto-pairs.vim'
    exec 'source ' $VIMHOME . '/plug-conf/sandwich.vim'

    exec 'source ' $VIMHOME . '/plug-conf/prettier.vim'
    exec 'source ' $VIMHOME . '/plug-conf/indentLine.vim'

    exec 'source ' $VIMHOME . '/plug-conf/multiple-cursor.vim'

    " exec 'source ' $VIMHOME . '/plug-conf/telescope.vim'
    exec 'source ' $VIMHOME . '/plug-conf/fzf.vim'
" -------------------------------------------------------------------
" SELECT YOUR THEME HERE -- select two lines -- Visual plugins config{{{
    exec 'source ' $VIMHOME . '/plug-conf/theme-sonokai.vim'
    colorscheme sonokai

  " source $HOME/.config/nvim/plug-conf/theme-ayu.vim
  " colorscheme ayu

  " source $HOME/.config/nvim/plug-conf/theme-gruvbox.vim
  " colorscheme gruvbox-material

  " source $HOME/.config/nvim/plug-conf/theme-gruvbox.vim
  " colorscheme gruvbox-material

  " runtime ./colors/molokai.vim
  " colorscheme  molokai

  " colorscheme monokai
  " colorscheme edge

  " }}}
